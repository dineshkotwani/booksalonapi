import { Navbar,Container } from 'react-bootstrap';
import './App.css';

function App() {
  return (
      <div className="App">
        <Navbar bg="light" expand="lg" className="NavBar" title='Testing app'>
          <Container>
            <Navbar.Brand className='NavBar'>AR Salon and Day Spa Services</Navbar.Brand>
          </Container>
        </Navbar>
        <header className="App-header">
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
        </header>
      </div>
  );
}

export default App;
