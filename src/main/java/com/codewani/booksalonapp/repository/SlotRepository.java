package com.codewani.booksalonapp.repository;

import com.codewani.booksalonapp.model.Slot;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SlotRepository extends JpaRepository<Slot,Long> {
}
