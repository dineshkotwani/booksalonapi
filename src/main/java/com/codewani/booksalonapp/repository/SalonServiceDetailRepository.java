package com.codewani.booksalonapp.repository;

import com.codewani.booksalonapp.model.SalonServiceDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalonServiceDetailRepository extends JpaRepository<SalonServiceDetail,Long> {
}
