package com.codewani.booksalonapp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
@PropertySource("classpath:application.properties")
public class SalonConfig {
    @Value("${name}")
    private String name;

    @Value("${address}")
    private String address;

    @Value("${city}")
    private String city;

    @Value("${state}")
    private String state;

    @Value("${zipcode}")
    private String zipcode;

    @Value("${phone}")
    private String phone;


    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }
}
