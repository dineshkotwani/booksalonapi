package com.codewani.booksalonapp.model;

public enum SlotStatus {
    AVAILABLE,
    LOCKED,
    CONFIRMED,
    CANCELLED
}
