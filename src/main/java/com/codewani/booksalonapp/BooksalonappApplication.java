package com.codewani.booksalonapp;

import com.codewani.booksalonapp.model.Slot;
import com.codewani.booksalonapp.repository.SlotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.PostConstruct;
import java.util.List;

@SpringBootApplication
@EnableSwagger2
public class BooksalonappApplication {

    @Autowired
    private SlotRepository slotRepository;

    public static void main(String[] args) {
        SpringApplication.run(BooksalonappApplication.class, args);
    }

    @PostConstruct
    public void start(){
        List<Slot> allSlots = slotRepository.findAll();

    }

}
